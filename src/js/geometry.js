export default function omtrekCirkel(straal){
  return  straal * 2 * Math.PI;
}

export  function oppervlakCirkel(straal){
  return  straal * straal * Math.PI;
}
